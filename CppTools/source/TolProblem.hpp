/* TolProblem.hpp: API TOL Nomad (http://www.gerad.ca/nomad/Project/Home.html)

   Copyright (C) 2012, Bayes Decision, SL (Spain [EU])

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
   USA.
 */

#ifndef _TolProblem_

#define _TolProblem_

#include <nomad.hpp>

#if defined(_MSC_VER)
#  include <win_tolinc.h>
#endif

#include <tol/tol_bdatgra.h>
#include <tol/tol_bsetgra.h>
#include <tol/tol_bmatgra.h>
#include <tol/tol_bvmatgra.h>
#include <tol/tol_btxtgra.h>
#include <tol/tol_bcodgra.h>
#include <tol/tol_bnameblock.h>
#include <tol/tol_btimer.h>



///////////////////////////////////////////////////////////////////////////////
class TolProblem 
/*
  Solves the non linear problem

  min f(x)
  subject to 
    g_k(x) <= 0 , k = 1 ... r
    l_x_j <= x_j <= u_x_j , j = 1 ... n

  Each x_j can be a real, integer, categorical or binary variable
  f(x) is an arbitrary function, continuous or not, even a non deterministic one
*/
///////////////////////////////////////////////////////////////////////////////
{
public:
  static int badFields_;
  static BDat code_addr( TolProblem& ptr );
  static TolProblem& decode_addr( BDat& hnd );
  static bool check_is_finite(BDat& v, const BText& name);
  static bool check_is_finite(BMat& v, const BText& name);
  static bool check_is_finite(BVMat& v, const BText& name);
  static BNameBlock& EnsureWrapper(BNameBlock& wrapper);
  BSyntaxObject* EnsureMember(const BText& name);

  NOMAD::Parameters nomad_;
  //TOL Instance of class TolNomad::@Problem
  const BNameBlock& wrapper_;
  //Handler to this TolProblem instance to be used in TOL
  BDat* _handler;

  //Domain dimension
  const BDat* n_; 
  int n;
  //Number of restrictions.
  const BDat* r_; 
  int r;
  //Sign of optimization: -1 for minimization, +1 for maximization
  const BDat* sign_; 

  //Numeric type of each variable
  const BSet* x_type;
  //Lower limit for variables l_x_j
  const BMat* x_lower;
  //Upper limit for variables l_x_j
  const BMat* x_upper;
  //Initial point
  const BMat* x_init;
  //Method that returns a vector column matrix ((r+1) x 1) with target f(x) 
  //and restrictions g_k(x)
  const BCode* nomad_target; 
  //Maximum number of evaluations
  const BDat* max_eval; 
  //Maximum elapsed time
  const BDat* max_time; 
  //Minimum mesh size relative to initial mesh
  const BDat* min_mesh_size_rel; 
  //Minimum poll size relative to initial mesh
  const BDat* min_poll_size_rel; 
  //Elapsed time in seconds between status traces
  const BDat* trace_time; 
  //Cache file; if the file does not exist, it will be created
  const BText* cache_file; 
  //The cache fies are saved every this number of iterations
  const BDat* cache_save_period; 
  //Nomad terminates if the cache reaches this memory limit expressed in MB
  const BDat* cache_memory; 
//const BDat* cache_search;

  //Constrained optimal point
  BMat* x_opt_constrained;
  BDat* f_opt_constrained;
  BMat* g_opt_constrained;
  //Unconstrained optimal point
  BMat* x_opt_unconstrained;
  BDat* f_opt_unconstrained;
  BMat* g_opt_unconstrained;

  BText* stop_desc;
  BSet* stats_report;

  //True if no problems has been detected
  bool isGood_;
  //True if problem has been initialized
  bool isInitialized;
  //Number of runned evaluations
  int numEval;
  int numObj;

  //internal user matrix to handle with argument of evaluation
 	BUserMat* uX;

  BTimer timer;
  double lastMsgClock;
  NOMAD::Mads* mads_;

  TolProblem(BNameBlock& wrapper);
  virtual ~TolProblem();
  void set_best_constrained(const NOMAD::Eval_Point& bf);
  void set_best_unconstrained(const NOMAD::Eval_Point& bf);
  void check_best(const NOMAD::Eval_Point& point);
  void get_mads_best(void);
  bool eval_x ( NOMAD::Eval_Point& x,
    const NOMAD::Double & h_max,
    bool                & count_eval   ) const;
  bool initialize();
  bool optimize();
};


#endif /*_TolProblem_*/