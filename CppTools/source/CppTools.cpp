/* CppTools.cpp: API TOL Nomad (http://www.gerad.ca/nomad/Project/Home.html)

   Copyright (C) 2012, Bayes Decision, SL (Spain [EU])

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
   USA.
 */


//Starts local namebock scope
#define LOCAL_NAMEBLOCK _local_namebtntLock_

#include <nomad.hpp>

#if defined(_MSC_VER)
#  include <win_tolinc.h>
#endif

#include <tol/tol_LoadDynLib.h>
#include <tol/tol_bcommon.h>
#include <tol/tol_btxtgra.h>
#include <tol/tol_bdatgra.h>
#include <tol/tol_bmatgra.h>
#include <tol/tol_bcodgra.h>
#include <tol/tol_boper.h>
#include <tol/tol_bnameblock.h>
#include <tol/tol_blanguag.h>
#include "TolProblem.hpp"

//Creates local namebtntLock container
static BUserNameBlock* _local_unb_ = NewUserNameBlock();

//Creates the reference to local namebtntLock
static BNameBlock& _local_namebtntLock_ = _local_unb_->Contens();

//Entry point of library returns the NameBlock to LoadDynLib
//This is the only one exported function 
DynAPI void* GetDynLibNameBlockTolNomad()
{
  BUserNameBlock* copy = NewUserNameBlock();
  copy->Contens() = _local_unb_->Contens();
  return(copy);
}

#define ERR(cond,msg,ret) \
if(cond) { \
  Error(_MID<<msg); \
  return(ret); \
}

NOMAD::Display out ( std::cout );



//--------------------------------------------------------------------
DeclareContensClass(BDat, BDatTemporary, 
  BDat_create_nomad_problem);
DefMethod(1, BDat_create_nomad_problem, 
  "create_nomad_problem", 1, 1, "NameBlock",
  "(NameBlock problem)",
  "Creates the internal C++ instance of the NOMAD problem solver.\n",
  BOperClassify::MatrixAlgebra_);
void BDat_create_nomad_problem::CalcContens()
//--------------------------------------------------------------------
{
  static BText _MID = "[TolNomad::CppTools::create_nomad_problem] ";
//Std(BText("\nTRACE ")<<_MID<<" 1");
  BUserNameBlock* unb = UNameBlock(Arg(1));
//Std(BText("\nTRACE ")<<_MID<<" 2");
  BNameBlock& nb = unb->Contens();
//Std(BText("\nTRACE ")<<_MID<<" 3");
  TolProblem* tp_ptr = new TolProblem(nb);
  *(tp_ptr->_handler) = TolProblem::code_addr(*tp_ptr);
//Std(BText("\nTRACE ")<<_MID<<" 4");
  contens_ = tp_ptr->isGood_;
}

//--------------------------------------------------------------------
DeclareContensClass(BDat, BDatTemporary, 
  BDat_destroy_nomad_problem);
DefMethod(1, BDat_destroy_nomad_problem, 
  "destroy_nomad_problem", 1, 1, "Real",
  "(Real handler)",
  "Destroys the internal C++ instance of an NOMAD problem solver\n",
  BOperClassify::MatrixAlgebra_);
void BDat_destroy_nomad_problem::CalcContens()
//--------------------------------------------------------------------
{
  static BText _MID = "[TolNomad::CppTools::destroy_nomad_problem] ";
//Std(BText("\nTRACE")+" TolNomad::CppTools::destroy_nomad_problem 1\n");
  BDat& _handler = Dat(Arg(1));
  if(_handler && !_handler.IsUnknown())
  {
  //Std(BText("\nTRACE")+" TolNomad::CppTools::destroy_nomad_problem 2\n");
    TolProblem& nlp = TolProblem::decode_addr(_handler);
    TolProblem* nlp_ptr = &nlp;
  //Std(BText("\nTRACE")+" TolNomad::CppTools::destroy_nomad_problem 3\n");
    if((nlp_ptr!=NULL) && nlp.isGood_ && 
       nlp._handler->IsKnown() && (*nlp._handler==_handler))
    {
    //Std(BText("\nTRACE")+" TolNomad::CppTools::destroy_nomad_problem 4\n");
      delete &nlp;
    //Std(BText("\nTRACE")+" TolNomad::CppTools::destroy_nomad_problem 5\n");
      contens_ = true;
    }
    else 
    {
      Error(_MID<<"Invalid handler "+_handler+"\n");
      contens_ = false;
    }
  }
//Std(BText("\nTRACE")+" TolNomad::CppTools::destroy_nomad_problem 6\n");
}

//--------------------------------------------------------------------
DeclareContensClass(BDat, BDatTemporary, 
  BDat_nomad_initialize);
DefMethod(1, BDat_nomad_initialize, 
  "nomad_initialize", 1, 1, "Real",
  "(Real handler)",
  "Initializes the NOMAD application.\n",
  BOperClassify::MatrixAlgebra_);
void BDat_nomad_initialize::CalcContens()
//--------------------------------------------------------------------
{
  static BText _MID = "[TolNomad::CppTools::nomad_initialize] ";
  BDat& _handler = Dat(Arg(1));
  TolProblem& nlp = TolProblem::decode_addr(_handler);
  TolProblem* nlp_ptr = &nlp;
  if(nlp_ptr!=NULL)
  {
    contens_ = nlp.initialize();
  }
  else
  {
    Error(_MID<<"Invalid handler "+_handler+"\n");
    contens_ = false;
  }
}

//--------------------------------------------------------------------
DeclareContensClass(BDat, BDatTemporary, 
  BDat_nomad_optimize);
DefMethod(1, BDat_nomad_optimize, 
  "nomad_optimize", 1, 1, "Real",
  "(Real handler)",
  "Optimizes the NOMAD problem.\n",
  BOperClassify::MatrixAlgebra_);
void BDat_nomad_optimize::CalcContens()
//--------------------------------------------------------------------
{
  static BText _MID = "[TolNomad::CppTools::nomad_optimize] ";
  BDat& _handler = Dat(Arg(1));
  TolProblem& nlp = TolProblem::decode_addr(_handler);
  TolProblem* nlp_ptr = &nlp;
  if(nlp_ptr!=NULL)
  {
    contens_ = nlp.optimize();
  }
  else
  {
    Error(_MID<<"Invalid handler "+_handler+"\n");
    contens_ = false;
  }
}

/* */
